import React from 'react';
import axios from "axios";
import {Profile} from "../Redux/storeTypes";
import {FilterType} from "../Redux/users-reducer";


const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        "API-KEY": "dcdc1053-cd01-41c7-8e1b-35fc256babcb",
    }

});
export const usersAPI = {
    getUsers(currentPage: number, pageSize: number,filter:FilterType) {
        return instance.get(`users?page=${currentPage}&count=${pageSize}&term=${filter.term}`+(filter.friend===null?"":`&friend=${filter.friend}`))
            .then(response => {
                return response.data
            })
    },
    follow(userId: number) {
        return instance.post(`follow/${userId}`, {} ).then(res=>res.data)
    },
    unfollow(userId: number) {
        return instance.delete(`follow/${userId}`).then(res=>res.data)
    },
    getProfile(userId:number){
        console.log("do not use this ,ethod")
        return profileAPI.getProfile(userId)
    }

}
export const profileAPI = {
    getProfile(userId:number){  return instance.get(`profile/` + userId)
    },
    getStatus(userId:number){  return instance.get(`profile/status/`+ {userId})
    },
    updateStatus(status:string){  return instance.put(`profile/status`+ {status:status})
    },
    savePhoto(photFile:any){
        const formData:any=new FormData()
        formData.append("image",photFile);
        return instance.put(  `profile/photo`, formData,{
            headers:{
                'Content-Type':'multipart/form-data'
            }
        })
    },
    saveProfile(profile:Profile){
        return instance.put(  `profile`, profile )
    }

}
export type MeResponseType={
    data: { id:number,email:string,login:string }
    resultCode:resultCodesEnum
    messages:Array<string>
}
export type LoginResponseType={
    data: { userId:number }
    resultCode:resultCodesEnum
    messages:Array<string>
}
export enum resultCodesEnum{
    Success,
    Error,
    captchaisRequired=10
}


export const authAPI = {
    me() {
        return instance.get<MeResponseType>(`auth/me`).then(res=>res.data);
    },
    login(email:string,password:string,rememberMe:string) {
        return instance.post<LoginResponseType>(`auth/login`,{email,password,rememberMe})
            .then(res=>res.data)

    },
    logout() {
        return instance.delete(`auth/login`);
        },


}
export const securityAPI = {
    getCaptchaUrl() {
        return instance.get(`security/get-captcha-url`);
    }

}

