import React from 'react';
import {ChatMessageType} from "../pages/Chat/ChatPage";

const subcribers = {
    'message-recived':[] as MessagesResivedSubscriberType[],
    'status-changed':[] as StatusChjangedSubscriberType[]
}
type EventsNameType='message-recived'|'status-changed'
let ws: WebSocket
const closeHandler = () => {
    notifySubscribersAboutStatusType("pending")

    setTimeout(createChanel, 3000)
}
const notifySubscribersAboutStatusType = (e:StatusType) => {
    subcribers["status-changed"].forEach(s=>s(e))
}
const openHandler = () => {
    notifySubscribersAboutStatusType("ready")
}
const errorHandler = () => {
    notifySubscribersAboutStatusType("error")
    alert("Restart Page")
}
const messageHandler = (e: MessageEvent) => {
    const newMessage = JSON.parse(e.data)
    subcribers['message-recived'].forEach(s => s(newMessage))
};

function createChanel() {
    cleanUp()
    ws?.close()
    ws = new WebSocket("wss://social-network.samuraijs.com/handlers/ChatHandler.ashx")
    notifySubscribersAboutStatusType("pending")
    ws.addEventListener('close', () => closeHandler())
    ws.addEventListener('message',  messageHandler)
    ws.addEventListener('open',  openHandler)
    ws.addEventListener('error',  errorHandler)

}
const cleanUp=()=>{
    ws?.removeEventListener('close',closeHandler)
    ws?.removeEventListener('message',  messageHandler)
    ws?.removeEventListener('error',  errorHandler)
    ws?.removeEventListener('open',  openHandler)
}


export const chatAPI = {
    subscribe(eventName:EventsNameType,callback: MessagesResivedSubscriberType|StatusChjangedSubscriberType) {
//@ts-ignore
        subcribers[eventName].push(callback)
        return () => {
            //@ts-ignore
            subcribers[eventName] = subcribers[eventName].filter(s => s !== callback)
        }


    },
    unsubscribe(eventName:EventsNameType, callback: MessagesResivedSubscriberType|StatusChjangedSubscriberType) {
        //@ts-ignore
        subcribers[eventName] = subcribers[eventName].filter(s => s !== callback)
    },
    start() {
      createChanel()
    },
    stop() {
        subcribers['message-recived']=[]
        subcribers['status-changed']=[]
        cleanUp()
        ws?.close()

    },

    sendMessage(message: string) {
        ws?.send(message)
    }
}
type MessagesResivedSubscriberType = (messages: ChatMessageType[]) => void
type StatusChjangedSubscriberType = (status: StatusType) => void
export type StatusType='pending'|"ready"|"error"
