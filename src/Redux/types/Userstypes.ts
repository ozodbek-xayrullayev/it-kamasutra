
interface Photos{
    small:string|boolean
    large:string|boolean
}
export interface ItemsType{
    id:number
    name:string
    uniqueUrlName:null|string
    followed:boolean
    status:string|null
    photos:Photos
}

export interface UsersType{
    items:ItemsType[]
    totalCount?:number
    error?:null|number

}
export interface InState{
    users:UsersType[]
}
let initialState:InState={
    users:[

    ]

}