import {createSelector} from "reselect";
import {AppStateType} from "./redux-store";

export const getUsersList=(state:AppStateType)=>{
    return state.usersPage.users
}
export const getUserSuperSelector=createSelector(getUsersList,(users:any)=>{
    return users.filter((u:any)=>true)
})
export const getTotalUsersCount=(state: AppStateType)=>{
    return state.usersPage.totalCount
}
export const getPageSize=(state: AppStateType)=>{
    return state.usersPage.pageSize
}
export const getFilterParam=(state: AppStateType)=>{
    return state.usersPage.filter
}
export const getCurrentPage=(state: AppStateType)=>{
    return state.usersPage.currentPage
}
export const getIsFetching=(state: AppStateType)=>{
    return state.usersPage.isFetching
}
export const getFollowingIsFetching=(state: AppStateType)=>{
    return state.usersPage.followingIsFetching
}
export const countSomethisngDifficult=(state: AppStateType)=>{
    let count=23
    return count
}