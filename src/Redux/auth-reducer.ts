import {LoginState} from "./storeTypes";
import {TodoAction, TodoActionType} from "./types";
import {authAPI, resultCodesEnum, securityAPI} from "../api/api";
import {stopSubmit} from "redux-form";


let initialState: LoginState = {
    userId: null,
    email: null,
    login: null,
    isAuth: false,
    captcha:null

};
const authReducer = (state = initialState, action: TodoAction) => {
    switch (action.type) {


        case TodoActionType.SET_USER_DATA:
            return {
                ...state,
                ...action.data,
                // isAuth:action.data.isAuth

            }
        default:
            return state
    }
}
export const setAuthUserData = (userId: any, email: any, login: any, isAuth: boolean) => ({
    type: TodoActionType.SET_USER_DATA,
    data: {userId, email, login, isAuth}
})
export const getCaptchaUrlSuccess = (captchaUrl:string) => ({
    type: TodoActionType.GET_CAPTCHA_URL_SUCCESS,
    action: {captchaUrl}
})
export const  getAuthUserData = () =>async (dispatch: any) => {
    let response=await  authAPI.me()
    if (response.resultCode === 0) {
        let {id, email, login}= response.data
        dispatch(setAuthUserData(id, email, login, true));
    }
}
export const login = (email: string, password: string, rememberMe: string) => async (dispatch: any) => {

   let response=await authAPI.login(email, password, rememberMe)
    if (response.resultCode === resultCodesEnum.Success) {
        dispatch(getAuthUserData())

    } else {
    if(response.resultCode === resultCodesEnum.captchaisRequired){
        dispatch(getCaptchaUrl())
        }
        let messages=response.messages.length>0?response.messages: "Email or password are wrong"
        dispatch( stopSubmit("login", {_error:messages}));
    }
}

export const getCaptchaUrl = () => async (dispatch: any) => {
   let response=await securityAPI.getCaptchaUrl()
    const capthcaUrl=response.data.url;
   dispatch(getCaptchaUrlSuccess(capthcaUrl))
}
export const logout = () => async (dispatch: any) => {
    let response=await authAPI.logout()
    if (response.data.resultCode === 0) {
        dispatch(setAuthUserData(null, null, null, false))

    }
}

export default authReducer