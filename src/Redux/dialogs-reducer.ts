import {TodoAction, TodoActionType} from "./types";
import {Dialog} from "./storeTypes";


let initialState:Dialog={
    dialogData: [
        {id: "1", name: "Dimich"},
        {id: "2", name: "OZOD"},
        {id: "3", name: "ALi"},
        {id: "4", name: "nozliya"},
        {id: "5", name: "Durdona"},
        {id: "6", name: "Madina"}
    ],
    messageData: [
        {id: "1", message: "Hi"},
        {id: "2", message: "HI how are you"},
        {id: "3", message: "it is mee"},
    ],
}
const dialogReducer=(state=initialState,action:TodoAction)=> {
    let stateCopy = {
        ...state,
        messageData: [...state.messageData],
    }
    switch (action.type) {
        case TodoActionType.SEND_MESSAGE_TEXT:
            let body = action.newMessageBody
            stateCopy = {
                ...state,
                messageData: [
                    ...state.messageData,
                    {id: "123", message: body}
                ],
            }
            return stateCopy

        default:
            return state

    }

}
export  const sendMessageCreator = (newMessageBody:string) => ({
    type: TodoActionType.SEND_MESSAGE_TEXT,
    newMessageBody
})

export default dialogReducer