import {combineReducers, createStore} from "redux";
import profileReducer from "./profile-reducer";
import dialogReducer from "./dialogs-reducer";
import sidebarReducer from "./sidebar-reducer";
import usersReducer from "./users-reducer";
import authReducer from "./auth-reducer";
import appReducer from "./app-reducer";
import thunk from "redux-thunk"
import {applyMiddleware} from "redux"
import {reducer as formReducer} from "redux-form";
import chatReducer from "./chat-reducer";

let  rootReducer=combineReducers({
    profilePage:profileReducer,
    dialogPage:dialogReducer,
    sidebar:sidebarReducer,
    usersPage:usersReducer,
    auth:authReducer,
    form:formReducer,
    app:appReducer,
    chat:chatReducer
});
type RootReducerType=typeof rootReducer
export type AppStateType=ReturnType<RootReducerType>

type Properties<T> =T extends {[key:string]:infer U}?U:never
export type InferActionTypes<T extends {[key:string]:(...args:any[])=>any}>=ReturnType<Properties<T>>


// const composeEnhancers=window.__REDUX_DEVTOOLS_EXTENTION_COMPOSE__||compose;
let store=createStore( rootReducer, applyMiddleware(thunk));

export default store