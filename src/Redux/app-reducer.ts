import {LoginState} from "./storeTypes";
import {TodoAction, TodoActionType} from "./types";
import {authAPI} from "../api/api";
import {stopSubmit} from "redux-form";
import React from "react";
import {getAuthUserData} from "./auth-reducer";

interface InitializedState{
    initialized:boolean
}
let initializedState:InitializedState = {
    initialized: false,
};
const appReducer = (state = initializedState, action: TodoAction) => {
    switch (action.type) {
        case TodoActionType.INITIALING_SUCCES:
            return {
                ...state,
                initialized:true
            }
        default:
            return state
    }
}
export const initializedSucces = () => ({
    type: TodoActionType.INITIALING_SUCCES,
})
export const initializeApp = () => (dispatch: any) => {
let   promise=dispatch(getAuthUserData())
    // dispatch(getAuthUserData())
    Promise.all([promise]).then(()=>{
        dispatch(initializedSucces())


    })
// dispatch(getAuthUserData())
}
export default appReducer