import {TodoAction, TodoActionType} from "./types";
import {ChatMessageType} from "../pages/Chat/ChatPage";
import {chatAPI, StatusType} from "../api/chat-api";
import { v4 as uuidv4 } from 'uuid';
type ChatMessageIdType=ChatMessageType & {id:string}
let initialState = {
    messages: [] as ChatMessageIdType[],
    status: "ready" as StatusType,

};
const chatReducer = (state = initialState, action: TodoAction) => {
    switch (action.type) {
        case TodoActionType.MESSAGES_RECIVED:
            return {
                ...state,
                messages: [...state.messages, ...action.payload.map(m=>({...m,id:uuidv4()}))].filter((m:any,i,a)=>i>=a.length-50)

            }
        case TodoActionType.SEND_CHAT_STATUS:
            return {
                ...state,
                status: action.payload

            }
        default:
            return state
    }
}

export const actions = {
    messagesRessived: (messages?: ChatMessageType[]) => ({

        type: TodoActionType.MESSAGES_RECIVED,
        payload: messages
    } as const)  ,
    statusChanged: (status?: StatusType) => ({
        type: TodoActionType.SEND_CHAT_STATUS,
        payload: status
    } as const)
}

let _newMessageHandler: ((messages: ChatMessageType[]) => void) | null = null
const newMessageHandlerCreater = (dispatch: any) => {
    if (_newMessageHandler === null) {
        _newMessageHandler = (messages: ChatMessageType[]) => {
            dispatch(actions.messagesRessived(messages))
        }
    }

    return _newMessageHandler
}
let _statusChangerHandler: ((status: StatusType) => void) | null = null
const statusChangerHandlerCreater = (dispatch: any) => {
    if (_statusChangerHandler === null) {
        _statusChangerHandler = (status) => {
            dispatch(actions.statusChanged(status))
        }
    }

    return _statusChangerHandler
}
export const startMessagesListening = () => async (dispatch: any) => {
    chatAPI.start()
    await chatAPI.subscribe("message-recived",newMessageHandlerCreater(dispatch))
    chatAPI.subscribe("status-changed", statusChangerHandlerCreater(dispatch))
}
export const stopMessagesListening = () => async (dispatch: any) => {
    await chatAPI.unsubscribe("message-recived",newMessageHandlerCreater(dispatch))
    chatAPI.unsubscribe("status-changed", statusChangerHandlerCreater(dispatch))
    chatAPI.stop()
}
export const sendMessagesListening = (message: string) => async (dispatch: any) => {
    await chatAPI.sendMessage(message)

}


export default chatReducer