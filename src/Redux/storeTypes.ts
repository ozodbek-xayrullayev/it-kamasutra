import {InState} from "./users-reducer";
import {ChatMessageType} from "../pages/Chat/ChatPage";
import {StatusType} from "../api/chat-api";

export interface Posts{
    name?:string
    id:string
    message?:string
    likesCount?:number
}
export interface ChatType{
    messages:ChatMessageType[],
    status: StatusType,
}
export interface Profile{
    newPostText: string
    posts: Posts[],
    profile:any,
    status:string
}

export interface LoginState{
    userId:null|number,
    email:null|string,
    login:null|string,
    captcha?:any,
    isAuth?:boolean
};
export interface Dialog{
    dialogData:Posts[]
    messageData:Posts[]

}
export interface App{
    initialized:boolean

}
export interface State{
    profilePage:Profile
    dialogPage:Dialog
    sidebar:any
    usersPage:InState
    auth:LoginState,
    app:App,
    chat:ChatType,

}
//
// export interface Store{
//     _state:State
//     _callSubscribber(state:State):void
//     subscribe:any
//     getState:any
//     dispatch:any
// }
//
// let store:Store={
//     _state: {
//         profilePage: {
//             newPostText: "It kamasutra",
//             posts: [
//                 {id: "1", likesCount: 3, message: "how are you"},
//                 {id: "2", likesCount: 4, message: "Itis my first post"},
//                 {id: "3", likesCount: 6, message: "it is mee"},
//                 {id: "4", likesCount: 3, message: "it is sadfassdf"},
//             ]
//         },
//         dialogPage: {
//             dialogData: [
//                 {id: "1", name: "Dimich"},
//                 {id: "2", name: "OZOD"},
//                 {id: "3", name: "ALi"},
//                 {id: "4", name: "nozliya"},
//                 {id: "5", name: "Durdona"},
//                 {id: "6", name: "Madina"}
//             ],
//             messageData: [
//                 {id: "1", message: "Hi"},
//                 {id: "2", message: "HI how are you"},
//                 {id: "3", message: "it is mee"},
//             ],
//             newMessageBody: "sdfsa"
//         },
//         sidebar: {}
//     },
//     getState() {
//         return this._state
//     },
//     _callSubscribber() {
//         console.log("State changed")
//     },
//     subscribe(observer: any) {
//         store._callSubscribber = observer;
//     },
//     dispatch(action: TodoAction) {
//         this._state.profilePage = profileReducer(store._state.profilePage, action);
//         this._state.dialogPage = dialogReducer(store._state.dialogPage, action);
//         store._callSubscribber(store._state)
//
//     }
// }
//
// export default store

