import {TodoAction, TodoActionType} from "./types";
import {Profile} from "./storeTypes";
import {profileAPI, usersAPI} from "../api/api";
import {stopSubmit} from "redux-form";

let initialStateProfile: Profile = {
    newPostText: "It kamasutra",
    posts: [
        {id: "1", likesCount: 3, message: "how are you"},
        {id: "2", likesCount: 4, message: "Itis my first post"},
        {id: "3", likesCount: 6, message: "it is mee"},
        {id: "4", likesCount: 3, message: "it is sadfassdf"},
    ],
    profile: null,
    status: "",
}

const profileReducer = (state = initialStateProfile, action: TodoAction) => {
    let stateCopy = {
        ...state,
        posts: [...state.posts]
    }
    switch (action.type) {
        case TodoActionType.ADD_POST:
            let message = action.newPostText
            console.log(state.posts)
            stateCopy = {
                ...state,
                posts: [...state.posts,
                    {id: "6", message: message}]
            }
            return stateCopy

        case TodoActionType.SET_USER_PROFILE:
            return {
                ...state,
                profile: action.profile
            }
        case TodoActionType.SAVE_PHOTO_SUCCES:
            return {
                ...state,
                profile: {...state.profile,photos:action.file}
            }
        case TodoActionType.SET_STATUS:
            return {

                ...state,
                status: action.status
            }

        default :
            return state

    }

}
export const addPostActionCreator = (newPostText: string) => ({
    type: TodoActionType.ADD_POST,
    newPostText
})
export const setUsersProfile = (profile: any) => ({
    type: TodoActionType.SET_USER_PROFILE,
    profile: profile
})
export const setStatus = (status: string) => ({
    type: TodoActionType.SET_STATUS,
    status: status
})
export const deletePost = (postId: number) => ({
    type: TodoActionType.SET_STATUS,

})
export const savePhotoSucces = (photos: any) => ({
    type: TodoActionType.SAVE_PHOTO_SUCCES,
    file:photos

})
export const getUsersProfile = (userId: number) => async (dispatch: any) => {
    let response = await usersAPI.getProfile(userId)
    dispatch(setUsersProfile(response.data))
}
export const getStatus = (userId: number) => async (dispatch: any) => {
    let response = await profileAPI.getStatus(userId)
    dispatch(setStatus(response.data))
}
export const savePhoto = (file:any) => async (dispatch: any) => {
    let response = await profileAPI.savePhoto(file)
    if (response.data.resultCode === 0) {
        dispatch(savePhotoSucces(response.data.data.photos))

    }


}
export const saveProfile = (file:any) => async (dispatch: any,getState:any) => {
    let userId=getState().auth.userId
    let response = await profileAPI.saveProfile(file)
    if (response.data.resultCode === 0) {

        dispatch(getUsersProfile(userId))
    }


}
export const updateStatus = (status: string) => async (dispatch: any) => {
    let response = await profileAPI.updateStatus(status)
    if (response.data.resultCode === 0) {
        dispatch(savePhoto(response.data))
    }
    else {
        dispatch( stopSubmit("edit-profile", {"keys":response.data.messages[0]}));

    }
}

export default profileReducer