import {UsersType} from "./users-reducer";
import {LoginState} from "./storeTypes";
import {ChatMessageType} from "../pages/Chat/ChatPage";
import {StatusType} from "../api/chat-api";

export enum TodoActionType {
    ADD_POST = "ADD_POST",
    SEND_MESSAGE_TEXT = "SEND_MESSAGE_TEXT",
    FOLLOW = "FOLLOW",
    UNFOLLOW = "UNFOLLOW",
    SET_USERS = "SET_USERS",
    SET_CURRENT_PAGE = "SET_CURRENT_PAGE",
    SET_TOTAL_COUNT = "SET_TOTAL_COUNT",
    TOGLE_IS_FETCHING = "TOGLE_IS_FETCHING",
    SET_USER_PROFILE = "SET_USER_PROFILE",
    GET_CAPTCHA_URL_SUCCESS = "samurai-network/GET_CAPTCHA_URL_SUCCESS",
    SET_USER_DATA = "samurai-network/auth/SET_USER_DATA",
    TOOGLE_FOLLOWING_PROGRESS = "TOOGLE_FOLLOWING_PROGRESS",
    SET_STATUS = "SET_STATUS",
    INITIALING_SUCCES = "INITIALING_SUCCES",
    SAVE_PHOTO_SUCCES = "SAVE_PHOTO_SUCCES",
    FAKE = "FAKE",
    SET_FILTER = "SET_FILTER",
    MESSAGES_RECIVED = "SN/chat?Messages_RECCIVED",
    SEND_CHAT_STATUS = "SN/chat?SEND_CHAT_STATUS",
}

interface FetchTodoAction {
    type: TodoActionType.ADD_POST
    newPostText?: string;
}
interface RecivedMessages {
    type: TodoActionType.MESSAGES_RECIVED
    payload:ChatMessageType[] ;
}
interface SendChatStatus {
    type: TodoActionType.SEND_CHAT_STATUS,
    payload:StatusType;
}


interface getStatusAction {
    type: TodoActionType.SET_STATUS;
    status: string;
}

interface savePhotoAction {
    type: TodoActionType.SAVE_PHOTO_SUCCES;
    file: any;
}


interface SendMessageAction {
    type: TodoActionType.SEND_MESSAGE_TEXT
    newMessageBody: string

}

interface GetCaptchaURLSuccess {
    type: TodoActionType.GET_CAPTCHA_URL_SUCCESS
    action: any

}

interface FollowAction {
    type: TodoActionType.FOLLOW
    UserId: number
}

interface UnFollowAction {
    type: TodoActionType.UNFOLLOW
    UserId: number

}

interface SetUsersAction {
    type: TodoActionType.SET_USERS
    users: UsersType[]
}

interface SetCurrentPageAction {
    type: TodoActionType.SET_CURRENT_PAGE
    currentPage: number
}

interface SetTotalCountAction {
    type: TodoActionType.SET_TOTAL_COUNT
    totalCount: number
}

interface TogleIsFetchingAction {
    type: TodoActionType.TOGLE_IS_FETCHING
}

interface TogleFollowingIsFetchingAction {
    type: TodoActionType.TOOGLE_FOLLOWING_PROGRESS
    userId: number,
    isFetching: boolean
}

interface SetProfiuleAction {
    type: TodoActionType.SET_USER_PROFILE
    profile: any
}

interface SetUserDataAction {
    type: TodoActionType.SET_USER_DATA
    data: LoginState
}

interface SetFilter {
    type: TodoActionType.SET_FILTER
    term: any
}

interface SetInitialized {
    type: TodoActionType.INITIALING_SUCCES
    data: boolean
}

interface fake {
    type: TodoActionType.FAKE
}


export type TodoAction =
    FetchTodoAction
    | SendMessageAction
    | FollowAction
    | UnFollowAction
    | SetUsersAction
    | SetCurrentPageAction
    | SetTotalCountAction
    | TogleIsFetchingAction
    | SetProfiuleAction
    | SetUserDataAction
    | TogleFollowingIsFetchingAction
    | getStatusAction
    | SetInitialized
    | fake
    | savePhotoAction
    | SetFilter
    | GetCaptchaURLSuccess
    | RecivedMessages
    | SendChatStatus
