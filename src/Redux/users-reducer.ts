import {TodoAction, TodoActionType} from "./types";
import {usersAPI} from "../api/api";
import {updateObjectInArray} from "../utils/objects-helpers";
import {Dispatch} from "react";
import {ActionTypes} from "redux-form";
import {InferActionTypes} from "./redux-store";

interface Photos {
    small: boolean | null
    large: boolean | null
}

export interface ItemsType {
    id: number
    name: string
    uniqueUrlName?: string | null
    followed?: boolean
    status: string | null
    photos: Photos
}

export interface UsersType {
    items: ItemsType[]
    totalCount?: number
    error?: null | number

}

export interface InState {
    users: ItemsType[]
    pageSize: number
    totalCount: number
    currentPage: number
    isFetching: boolean
    followingIsFetching: number[],
    fake: number
    filter: { term:string, friend:null|boolean}
}

let initialState: InState = {
    users: [],
    pageSize: 10,
    totalCount: 15,
    currentPage: 1,
    isFetching: false,
    followingIsFetching: [],
    fake: 10,
    filter: {term:"",friend:null}
}

export type FilterType = typeof initialState.filter

const usersReducer = (state = initialState, action: ActionsTypes) => {
    switch (action.type) {

        case TodoActionType.FOLLOW:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.UserId, "id", {followed: true},)

            }
        case TodoActionType.UNFOLLOW:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.UserId, "id", {followed: false},)


            }
        case TodoActionType.SET_USERS:
            return {
                ...state,
                users: [...action.users]
            }
        case TodoActionType.SET_CURRENT_PAGE:
            console.log(
                action.currentPage
            )
            return {
                ...state,
                currentPage: action.currentPage
            }
        case TodoActionType.SET_TOTAL_COUNT:
            return {
                ...state,
                totalCount: action.totalCount
            }
        case TodoActionType.TOGLE_IS_FETCHING:
            return {
                ...state,
                isFetching: !state.isFetching
            }
        case TodoActionType.SET_FILTER:
            return {
                ...state,
                filter: action.payload

            }
        case TodoActionType.TOOGLE_FOLLOWING_PROGRESS:
            return {
                ...state,
                followingIsFetching: action.isFetching
                    ? [...state.followingIsFetching, action.userId]
                    : state.followingIsFetching.filter((id: number) => (id !== action.userId))
            }
        default :
            return state
    }

}
type ActionsTypes = InferActionTypes<typeof actions>
export const actions = {
    followSuccess: (UserId: number) => ({
        type: TodoActionType.FOLLOW,
        UserId
    } as const),
    unfollowSuccess: (UserId: number) => ({
        type: TodoActionType.UNFOLLOW,
        UserId
    } as const),
    setUsers: (users: UsersType[]) => ({
        type: TodoActionType.SET_USERS,
        users
    } as const),
    setCurrentPageUsers: (currentPage: number) => ({
        type: TodoActionType.SET_CURRENT_PAGE,
        currentPage
    } as const),
    setFilter: (filter: FilterType) => ({
        type: TodoActionType.SET_FILTER,
        payload: filter
    } as const),
    setTotalUsersCount: (totalCount: number) => ({
        type: TodoActionType.SET_TOTAL_COUNT,
        totalCount
    } as const),
    TogleIsFetching: () => ({
        type: TodoActionType.TOGLE_IS_FETCHING,
    } as const),
    TogleFollowingProgres: (userId: number, isFetching: boolean) => ({
        type: TodoActionType.TOOGLE_FOLLOWING_PROGRESS,
        userId,
        isFetching
    } as const)


}
export const setCurrentPageUsers = (currentPage: number) => ({
    type: TodoActionType.SET_CURRENT_PAGE,
    currentPage
})

export const countSomethingDifficult = () => {
    let count = 23;
    return count;
}

export const getUsers = (currentPage: number, pageSize: number,filter:FilterType) => {
    return async (dispatch: any) => {
        dispatch(actions.TogleIsFetching())
        let data = await usersAPI.getUsers(currentPage, pageSize,filter);
        dispatch(actions.TogleIsFetching())
        dispatch(actions.setUsers(data.items))
        dispatch(actions.setFilter(filter))
        dispatch(actions.setTotalUsersCount(data.totalCount))
    }
}
const followUnfollowFlow = async (dispatch: any, userId: number,
                                  apiMethod: (userId: number) => Promise<any>, actionCreator: any) => {
    dispatch(actions.TogleFollowingProgres(userId, true));
    let response = await apiMethod(userId)
    console.log(response)
    if (response.resultCode == 0) {
        dispatch(actionCreator(userId));
    } else {
        dispatch(actions.TogleFollowingProgres(userId, false))
    }
    dispatch(actions.TogleFollowingProgres(userId, false));
}

export const follow = (userId: number) => {
    return async (dispatch: any) => {
        await followUnfollowFlow(dispatch, userId, usersAPI.follow.bind(usersAPI), actions.followSuccess)
    }
}

export const unfollow = (userId: number) => {
    return async (dispatch: any) => {
        await followUnfollowFlow(dispatch, userId, usersAPI.unfollow.bind(usersAPI), actions.unfollowSuccess)
    }
}


export default usersReducer