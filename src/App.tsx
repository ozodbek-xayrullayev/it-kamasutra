import React, {Component} from 'react';
import "./App.scss"
import "bootstrap/dist/css/bootstrap.min.css"
import {State} from "./Redux/storeTypes";
import {connect} from "react-redux";
import {initializeApp} from "./Redux/app-reducer";
import {compose} from "redux"
import {Breadcrumb, Layout, Menu} from "antd";
import {LaptopOutlined, NotificationOutlined, UserOutlined} from "@ant-design/icons";
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import {Link, Route} from "react-router-dom";
import Loading from "./components/Loader/Loading";
import {UserPage} from "./components/Users/UsersContainer";
import {Login} from "./components/Login/Login";
import {Headers} from "./components/Header/Headers";

const {SubMenu} = Menu;
const { Content, Footer, Sider} = Layout;

const DialogsContainer = React.lazy(() => import("./components/Dialogs/DialogsContainer"));
const ProfileContener = React.lazy(() => import("./components/Profile/ProfileContener"));
const ChatPage = React.lazy(() => import("./pages/Chat/ChatPage"));


export interface Name {
    likesCount?: number
    id: string
    message: string
}

class App extends Component<any> {
    catchAllUnhandledErrors = (promiseRejectError: any) => {
        alert("Some Error Happenend")
    }

    componentWillUnmount() {
        window.addEventListener("unhandledrejection", this.catchAllUnhandledErrors)
    }

    constructor(props: any) {
        super(props);
    }

    componentDidMount() {
        window.addEventListener("unhandledrejection", this.catchAllUnhandledErrors)
        this.props.initializeApp();
    }

    render() {
        if (!this.props.initialized) {
            return <span className='spinner-grow'/>
        }

        return (
            <Layout>
                <Headers/>
                <Content style={{padding: '0 50px'}}>
                    <Breadcrumb style={{margin: '16px 0'}}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>List</Breadcrumb.Item>
                        <Breadcrumb.Item>App</Breadcrumb.Item>
                    </Breadcrumb>
                    <Layout className="site-layout-background" style={{padding: '24px 0'}}>
                        <Sider className="site-layout-background" width={200}>
                            <Menu
                                mode="inline"
                                defaultSelectedKeys={['1']}
                                defaultOpenKeys={['sub1']}
                                style={{height: '100%'}}
                            >
                                <SubMenu key="sub1" icon={<UserOutlined/>} title="My profile">
                                    <Menu.Item key="1">
                                        <Link to='/dialogs'>Dialogs</Link></Menu.Item>
                                    <Menu.Item key="2"> <Link to='/profile'>Profile</Link></Menu.Item>
                                    <Menu.Item key="3">option3</Menu.Item>
                                    <Menu.Item key="4">option4</Menu.Item>
                                </SubMenu>
                                <SubMenu key="sub2" icon={<LaptopOutlined/>} title="Developers">
                                    <Menu.Item key="5"> <Link to='/developers'>Developers</Link></Menu.Item>
                                    <Menu.Item key="6"> <Link to='/loading'>Loading</Link></Menu.Item>
                                    <Menu.Item key="7">option7</Menu.Item>
                                    <Menu.Item key="8">option8</Menu.Item>
                                </SubMenu>
                                <SubMenu key="sub3" icon={<NotificationOutlined/>} title="subnav 3">
                                    <Menu.Item key="9"><Link to='/chat'>Chat</Link></Menu.Item>
                                    <Menu.Item key="10">option10</Menu.Item>
                                    <Menu.Item key="11">option11</Menu.Item>
                                    <Menu.Item key="12">option12</Menu.Item>
                                </SubMenu>
                            </Menu>
                        </Sider>
                        <Content style={{padding: '0 24px', minHeight: 280}}>

                            <Route path='/profile/:userId?' render={() => {
                                return <React.Suspense fallback={<div>Loading...</div>}>
                                    <ProfileContener/>
                                </React.Suspense>
                            }}/>
                            <Route exact={true} path='/'
                                   render={() => {
                                       return <React.Suspense fallback={<div>Loading...</div>}>
                                           <ProfileContener/>
                                       </React.Suspense>
                                   }}/>
                            <Route path='/dialogs'
                                   render={() => {
                                       return <React.Suspense fallback={<div>Loading...</div>}>
                                           <DialogsContainer/>
                                       </React.Suspense>

                                   }}/>
                            <Route path='/loading'
                                   render={() => {
                                       return <React.Suspense fallback={<div>Loading...</div>}>
                                           <Loading/>
                                       </React.Suspense>

                                   }}/>
                                   <Route path='/chat'
                                   render={() => {
                                       return <React.Suspense fallback={<div>Loading...</div>}>
                                           <ChatPage/>
                                       </React.Suspense>

                                   }}/>

                            <Route path='/developers'
                                   render={() => <UserPage
                                   />}/>
                            <Route exact={true} path='/login'
                                   render={() => <Login
                                   />}/>


                        </Content>
                    </Layout>

                </Content>
                <Footer style={{textAlign: 'center'}}>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>
            // <div className='app-wrappper container'>
            //     <Router>
            //         <HeaderContainer/>
            //         <div className='content'>
            //
            //             {/*<Route path='*'*/}
            //             {/*       render={() => <div>Page not found</div>}/>*/}
            //         </div>
            //     </Router>
            // </div>
        );
    }
}

const mapStateToPorps = (state: State) => ({
    initialized: state.app.initialized,
})
export default compose(connect(mapStateToPorps, {initializeApp}))(App);