import React, {useEffect, useRef, useState} from 'react';
import {Button} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {sendMessagesListening, startMessagesListening, stopMessagesListening} from "../../Redux/chat-reducer";
import {State} from "../../Redux/storeTypes";


export type ChatMessageType = {
    message: string
    photo: string
    userName: string
    userId: number
}

const ChatPage: React.FC = (props) => {
    return (
        <div>
            <Chat/>
        </div>
    );
}


const Chat: React.FC = () => {
    const dispatch = useDispatch()
    const status = useSelector((state: State) => state.chat.status)
    useEffect(() => {
        dispatch(startMessagesListening())
        return () => {
            dispatch(stopMessagesListening())
        }

    }, [])
    return <div>
        {status === "error" && <div>Some error is occured.
            please Refresh page
        </div>}
        <div>
            <Messages/>
            <AddMessageFrom/>
        </div>
    </div>

}

const Messages: React.FC = () => {
    const messages = useSelector((state: State) => state.chat.messages)
    const messagesAnchorRef = useRef<HTMLDivElement>(null)
    const [isAutoScroll, setIsAutoScroll] = useState(true)

    useEffect(() => {
        if (isAutoScroll) {
            messagesAnchorRef.current?.scrollIntoView({behavior: "smooth"})

        }
    }, [messages])
    const scrollHsndler = (e: React.UIEvent) => {
        var element = e.currentTarget
        if (element.scrollHeight - element.scrollTop <= element.clientHeight) {
            setIsAutoScroll(false)
            console.log(element.scrollHeight - element.scrollTop)
            console.log(element.clientHeight)
        } else {
            setIsAutoScroll(false)

        }
    }
    return <div style={{height: '400px', overflow: "scroll"}}
                onScroll={scrollHsndler}>
        {messages.map((item: any, key: number) => <Message message={item} key={item.id}/>)}
        <div ref={messagesAnchorRef}></div>
    </div>

}
const Message: React.FC<{ message: ChatMessageType }> = React.memo(({message}) => {
    console.log(message)

    return <div>
        <img style={{height: "30px", width: "30px"}} src={message.photo}/><b>{message.userName}</b>
        <br/>
        {message.message}
        <hr/>
    </div>
})
const AddMessageFrom: React.FC = () => {
    const [message, setMessage] = useState("")
    const dispatch = useDispatch()
    const status = useSelector((state: State) => state.chat.status)


    const sendMessageHandler = () => {
        dispatch(sendMessagesListening(message))
        setMessage("")
    }
    return <div>
        AddMessageFrom
        <div>
            <textarea
                value={message}
                onChange={(e) => setMessage(e.currentTarget.value)}
                name="chat" id="1" cols={20} rows={2}></textarea>
        </div>
        <div>
            <Button
                disabled={status !== 'ready'}
                onClick={sendMessageHandler}>Send</Button>
        </div>
    </div>


}
export default ChatPage
