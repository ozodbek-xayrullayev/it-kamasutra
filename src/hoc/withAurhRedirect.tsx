import React from 'react';
import {Redirect} from "react-router-dom";
import {State} from "../Redux/storeTypes";
import {connect} from "react-redux";


let mapStateToPropsForRedirec = (state: State) => ({
    isAuth:state.auth.isAuth
})

type Props = any
const WithAurhRedirect = (Component: any) => {

    class RedirectComponent extends React.Component <Props> {

        render() {

            if (!this.props.isAuth) {
                return <Redirect to="/login"/>
            }
            return <Component {...this.props}/>
        };
    }

    let ConnectedAuthRedirectComponent=connect(mapStateToPropsForRedirec)(RedirectComponent);

    return ConnectedAuthRedirectComponent
}

export default WithAurhRedirect;