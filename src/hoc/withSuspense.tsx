import React from 'react';


const withSuspense = (Component: any) => {
    return<React.Suspense fallback={<div>Loading...</div>}>
        <Component/>
    </React.Suspense>
}

export default withSuspense;