import React, {useEffect} from 'react';
import {FilterType, follow, getUsers, ItemsType, setCurrentPageUsers, unfollow} from "../../Redux/users-reducer";
import {FaUsers} from "react-icons/fa";
import {NavLink} from "react-router-dom"
import {UsersSearchForm} from "./UsersSearchForm";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom"
import {
    getCurrentPage,
    getFilterParam,
    getFollowingIsFetching,
    getIsFetching,
    getPageSize,
    getTotalUsersCount,
    getUserSuperSelector
} from "../../Redux/Users-selectors";
import * as querystring from "querystring";

type ParseCreateType = { term?: string, page?: string, friend?: string };
let Users = () => {

    const totalUsersCount = useSelector(getTotalUsersCount)
    const currentPage = useSelector(getCurrentPage)
    const pageSize = useSelector(getPageSize)
    const users = useSelector(getUserSuperSelector)
    const filter = useSelector(getFilterParam)
    const isFetching = useSelector(getIsFetching)
    const followingIsFetching = useSelector(getFollowingIsFetching)
    const dispatch = useDispatch();
    const history = useHistory();


    const onPageChanged = async (p: number) => {
        await dispatch(setCurrentPageUsers(p))
        // dispatch(getUsers(
        //     currentPage,
        //     pageSize, filter))
    }

    useEffect(() => {
        const parsed = querystring.parse(history.location.search.substr(1)) as ParseCreateType
        let actualPage = currentPage
        let actualFilter = filter
        if (!!parsed.page) actualPage = Number(parsed.page)
        if (!!parsed.term) actualFilter = {...actualFilter, term: parsed.term as string}
        switch (parsed.friend) {
            case "null":
                actualFilter = {...actualFilter, friend: null}
                break
            case "true":
                actualFilter = {...actualFilter, friend: true}
                break
            case "false":
                actualFilter = {...actualFilter, friend: false}
                break
        }

        dispatch(getUsers(
            actualPage,
            pageSize, filter))
    }, [])
    useEffect(() => {
        const query:ParseCreateType={}
        if(!!filter)query.term=filter.term
        if(filter.friend!==null)query.friend=String(filter.friend)
        if(currentPage!==1) query.page=String(currentPage)

        history.push({
            pathname: "/developers",
            search: querystring.stringify(query)
        })
    }, [filter, currentPage])


    const onFilerChanged = (filter: FilterType) => {
        dispatch(getUsers(1, pageSize, filter))
    }

    const unfollowUser = (userId: number) => {
        dispatch(unfollow(userId))
    }

    const followUser = (userId: number) => {
        dispatch(follow(userId))
    }


    let pageCount = totalUsersCount / pageSize
    let pages: number[] = []
    for (let i = 1; i <= pageCount; i++) {
        pages.push(i)
    }
    return (
        <div>
            <div>
                <UsersSearchForm onFilerChanged={onFilerChanged}
                />
                <div className='justify-content-center d-flex'>
                    {pages.map((p: number, i: number) => {
                        return (
                            <span key={i} onClick={() => {
                                onPageChanged(p)
                            }} className={`${(currentPage === p) && "selectedUserPage"} 
                       ${((currentPage !== p) &&
                                (p !== 1) &&
                                (parseInt((totalUsersCount / pageSize).toString()) !== p) &&
                                (currentPage - 1 !== p) &&
                                (currentPage - 2 !== p) &&
                                (currentPage - 50 !== p) &&
                                (currentPage + 1 !== p) &&
                                (currentPage + 2 !== p) &&
                                (currentPage + 50 !== p)
                            ) && "d-none"}
                      
                            btn btn-info 
                             btn-sm m-1`}>{p}</span>
                        )

                    })}

                </div>
                <div className='d-flex justify-content-center'>
                    {isFetching ? <span className='spinner-grow ml-2'/> : null}
                    {isFetching ? <span className='spinner-grow ml-2'/> : null}
                    {isFetching ? <span className='spinner-grow ml-2'/> : null}
                    {isFetching ? <span className='spinner-grow ml-2'/> : null}
                    {isFetching ? <span className='spinner-grow ml-2'/> : null}
                </div>
                {users.map((u: ItemsType) => <div key={u.id} className='d-flex p-3'>
                        <div className='w-25 '>
                            <NavLink to={'/profile/' + u.id}>
                                < FaUsers className='ml-4 mb-1'/>
                            </NavLink>
                            <div>
                                {u.followed ?
                                    <button disabled={followingIsFetching.some((id: number) => id === u.id)}
                                            className='btn btn-sm btn-danger'
                                            onClick={() => {
                                                unfollowUser(u.id)
                                            }}>Unfollow</button> :
                                    <button disabled={followingIsFetching.some((id: number) => id === u.id)}
                                            className='btn btn-sm btn-info'
                                            onClick={() => {
                                                followUser(u.id)
                                            }}>Follow</button>
                                }
                            </div>
                        </div>
                        <div className='w-75'>
                            <div className='d-flex justify-content-between border border-primary p-1'>
                                <div>
                                    <div>
                                        {u.name}
                                    </div>
                                    <div>
                                        {u.status ? u.status : "null"}
                                    </div>

                                </div>
                                <div className='text-right'>
                                    <div>
                                        {u.photos.large ? <img className='img-fluid' src={`${u.photos.large}`}
                                                               alt=""/> : "location country"}
                                    </div>
                                    <div>
                                        {u.photos.small ? <img className='img-fluid' src={`${u.photos.large}`}
                                                               alt=""/> : "location country"}
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                )}
            </div>

        </div>
    );
}


export default Users;
