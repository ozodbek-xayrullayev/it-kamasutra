import React from "react";
import {Form, Formik,Field} from "formik";
import {FilterType} from "../../Redux/users-reducer";
import {useSelector} from "react-redux";
import {getFilterParam} from "../../Redux/Users-selectors";

const UsersSearchValidate = (values: any) => {
    const errors = {};
    return errors;
}
type FriendType = "true" | "false" | "null";
type FormObjecType = {
    term: string
    friend:FriendType
}


export const UsersSearchForm = ({onFilerChanged}: { onFilerChanged: (filter: FilterType) => void }) => {
const filter=useSelector(getFilterParam)

    const submit = (values: FilterType, {setSubmitting}: { setSubmitting: any }) => {
        onFilerChanged(values)
        setSubmitting(false)
    }
    return (
        <div>
            <Formik
                enableReinitialize={true}
                initialValues={{term:filter.term, friend:(filter.friend) }}
                validate={UsersSearchValidate}
                onSubmit={submit}
            >
                {({
                      isSubmitting
                  }) => (
                    <Form>
                        <Field type="text" name="term"/>
                        <Field as="select" name="friend">
                            <option value="null">All</option>
                            <option value="true">Only friends</option>
                            <option value="false">Only unfolowed</option>
                        </Field>
                        <button type="submit"
                                disabled={isSubmitting}
                        >
                            FIND
                        </button>
                    </Form>
                )}
            </Formik>
        </div>
    );
}
