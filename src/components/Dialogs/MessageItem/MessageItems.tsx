import React from 'react';
    export interface Name{
    id:string
    message:string
}

const Message=(props:Name)=>{
    return <div className='active message  m-2'>{props.message}</div>
}

const MessageItems =(props:any)=> {

    let messageElements = props.messageData.map((dialog:Name) => {
        return <Message message={dialog.message} id={dialog.id}/>
    })

    return (
        <div>
            {messageElements}
        </div>
    );
}

export default MessageItems;