import React from 'react';
import "./Dialogs.scss";
import DialogItems from "./DialogItems/DialogItems";
import MessageItems from "./MessageItem/MessageItems";
import {Redirect} from "react-router-dom"
import {reduxForm, Field} from "redux-form";
import {Textarea} from "../common/FormsControls/FormConstrols";
import {maxLengthCreator, required} from "../../utils/validators/validators";

const Dialogs = (props: any) => {

    let addNewMessage = (e: any) => {

        props.sendMessage(e.newPostText)


    }
    if (!props.isAuth) return <Redirect to="/login"/>

    return (
        <div className='row'>
            <div className="col-4">
                <DialogItems dialogData={props.dialogPage.dialogData}/>

            </div>
            <div className='messages'>
                <MessageItems messageData={props.dialogPage.messageData}/>

            </div>
            <AddMessageFormRedux onSubmit={addNewMessage}/>
        </div>
    );
}
const maxLength10 = maxLengthCreator(100)


const AddMessageForm = (props: any) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field
                    placeholder="Post Message"
                    name="newPostText"
                    component={Textarea}
                    validate={[required, maxLength10]}

                />

            </div>
            <div>
                <button type="submit">SEND</button>
            </div>

        </form>
    )
}
const AddMessageFormRedux = reduxForm({form: "dialogAddMessageForm"})(AddMessageForm)

export default Dialogs;