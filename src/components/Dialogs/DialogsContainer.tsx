import React from 'react';
import "./Dialogs.scss";
import {sendMessageCreator} from "../../Redux/dialogs-reducer";
import Dialogs from "./Dialogs";
import {connect} from "react-redux";
import {State} from "../../Redux/storeTypes";
import WithAurhRedirect from "../../hoc/withAurhRedirect";




let mapDispatchToProps=(dispatch:any)=>{
    return{
        sendMessage:(newMessageBody:string)=>{
        dispatch(sendMessageCreator(newMessageBody))},


    }
}

class DialogsContainer extends React.Component {
    render() {
        return (
            <Dialogs {...this.props}/>

        );
    }
}


let mapStateToProps=(state:State)=>{
    return{
        dialogPage:state.dialogPage,

    }
}


let WithAurhRedirectDialig=WithAurhRedirect(DialogsContainer)

export default connect(mapStateToProps,mapDispatchToProps)(WithAurhRedirectDialig);
