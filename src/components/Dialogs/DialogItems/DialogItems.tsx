import React from 'react';
import {NavLink} from "react-router-dom";
export interface Name{
    name:string
    id:string
}



const DialogItem=(props:Name)=>{
    return <div className='active'>
        <NavLink activeClassName='active'  to={'/dialogs/'+props.id}>{props.name}</NavLink>
    </div>
}


const DialogItems =(props:any)=> {
    let dialogs = props.dialogData.map((dialog:Name) => {
        return <DialogItem name={dialog.name} id={dialog.id}/>
    })


    return (
      <div>
          {dialogs}
      </div>
    );
}

export default DialogItems;