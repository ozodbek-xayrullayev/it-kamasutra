import React from 'react';
import {reduxForm} from "redux-form";
import {createField, Textarea} from "../common/FormsControls/FormConstrols";
import {maxLengthCreator, required} from "../../utils/validators/validators";
import {useDispatch, useSelector} from "react-redux";
import {login} from "../../Redux/auth-reducer"
import {Redirect} from "react-router-dom";
import {State} from "../../Redux/storeTypes";

const maxLength10 = maxLengthCreator(30)

const LoginForm = (props: any) => {
    return (
        <form onSubmit={props.handleSubmit}>
            {createField("Login", "login", [required, maxLength10], Textarea, "text")}
            {createField("Pasword", "password", [required, maxLength10], Textarea, "password")}
            {createField(null, "rememberMe", [required, maxLength10], "input", "checkbox")}
            {props.error && <div className='border border-danger text-danger'>
                {props.error}
            </div>}

            <div>
                <button
                    onClick={props.onSubmit}
                    type={"submit"}>Login
                </button>
            </div>
        </form>

    );
}
const LoginReduxForm = reduxForm({form: 'login'})(LoginForm)
export const Login = () => {
    const captchaUrl = useSelector((state: State) => state.auth.captcha)
    const isAuth = useSelector((state: State) => state.auth.isAuth)
    const dispatch = useDispatch()
    const onSubmit = (formData: any) => {
        dispatch(login(formData.login, formData.password, formData.rememberMe))
    }
    if (isAuth) {
        return <Redirect to={"/profile"}/>
    }
    return (
        <div>
            <h1>
                LOGIN
            </h1>
            <LoginReduxForm
                onSubmit={onSubmit}
            />
            {captchaUrl && <img src={captchaUrl}/>}

        </div>
    );
}

