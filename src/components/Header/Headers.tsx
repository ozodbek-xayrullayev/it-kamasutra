import React from 'react';
import {Link} from "react-router-dom";
import {Avatar, Button, Col, Layout, Menu, Row} from "antd";
import {UserOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {selectCurrentLogin, selectIsAuth} from "../../Redux/auth-selectors";
import {logout} from "../../Redux/auth-reducer";

const {Header, Content, Footer, Sider} = Layout;

export const Headers = (props: any) => {
    const isAuth = useSelector(selectIsAuth)
    const login = useSelector(selectCurrentLogin)
    const dispatch = useDispatch()

    const logoutCallBack = () => {
        dispatch(logout())
    }

    return (
        <Header className="header">
            <div className="logo"/>
            <Row>
                <Col span={20}>
                    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                        <Menu.Item key="1"><Link to="/developers">Developers</Link></Menu.Item>
                    </Menu>

                </Col>
                <Col span={4}>
                    {isAuth ?
                        <div>
                            <Avatar className="mr-4" style={{backgroundColor: '#87d068'}} icon={<UserOutlined/>}/>
                            <Button onClick={logoutCallBack}>Log out</Button>
                        </div> : <Link to='profile'>
                            Login
                        </Link>}
                </Col>
            </Row>
        </Header>
    );
}

