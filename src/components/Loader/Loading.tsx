import React, {Component} from 'react';
import "./index.scss"
class Loading extends Component {
    render() {
        return (
           <div className="h-100 w-100 ">
              {/*<div className="row h-25 border border-danger">*/}
              {/*    <div className="col-5  border border-danger">*/}
              {/*        <div className="loading-component">*/}
              {/*            <span className="spinner-border"/>*/}
              {/*        </div>*/}
              {/*    </div>*/}
              {/*</div>*/}
               <div className="loading-component">
                   <span className="spinner-border"/>
               </div>
           </div>
        );
    }
}

export default Loading;