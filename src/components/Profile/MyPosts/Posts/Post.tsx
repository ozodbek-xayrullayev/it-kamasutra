import React from 'react';
import {FaUserTie} from "react-icons/fa";

const Post=(props:any)=> {
    return (
        <div className='item m-2 d-flex align-content-center'>
           <span className='mr-2'> <FaUserTie /></span>
            <span className='mr-2' >post 1 </span>
           <div> {props.message}</div>
           <div className='ml-3'>
               {props.likesCount}
           </div>
        </div>
    );
}

export default Post;