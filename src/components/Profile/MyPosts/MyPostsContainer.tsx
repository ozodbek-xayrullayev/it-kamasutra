import React from 'react';
import {addPostActionCreator} from "../../../Redux/profile-reducer";
import {State} from "../../../Redux/storeTypes";
import {connect} from "react-redux";
import MyPosts from "./MyPosts";



let mapStateToProps=(state:State)=>{
    return{
        posts:state.profilePage.posts,
        newPostText:state.profilePage.newPostText
    }
}
let mapDispatchToProps=(dispatch:any)=>{
    return{
        addPost:(newPostText:string)=>{
            dispatch(addPostActionCreator(newPostText))},

    }
}
const MyPostsContainer=connect(mapStateToProps,mapDispatchToProps)(MyPosts);



export default MyPostsContainer;