import React from 'react';
import Post from "./Posts/Post";
import {Name} from "../../../App";
import {Field, reduxForm} from "redux-form";
import {maxLengthCreator, required} from "../../../utils/validators/validators"
import {Textarea} from "../../common/FormsControls/FormConstrols";

const maxLength10 = maxLengthCreator(10)

function AddNewPost(props: any) {
    return <form onSubmit={props.handleSubmit}>
        <div>
            <Field
                placeholder="Post Message"
                name="newPostText"
                component={Textarea}
                validate={[required,maxLength10]}
            />

        </div>
        <button type={"submit"}>Add posts</button>
    </form>;
}

const AddNewPostForm = reduxForm({form: "ProfileAddNewPostForm"})(AddNewPost);

const MyPosts = React.memo((props: any) => {
    console.log("rendered")

    let postElements = props.posts.map((post: Name) => {
        return <Post message={post.message} id={post.id} likesCount={post.likesCount}/>
    })
    let newPostElement: any = React.createRef();
    const addPost = (values: any) => {
        props.addPost(values.newPostText);
    }
    let onPostChange = () => {
        let newText: string = newPostElement.current.value;
        props.updateNewPostText(newText);

    }
    return (
        <div>
            <div>
                <h2>My posts</h2>
                <AddNewPostForm onSubmit={addPost}/>
                <div className="posts">
                    {postElements}
                </div>
            </div>
        </div>
    );
});

// const AddNewPostForm=()=>{
//     return
//
// }

export default MyPosts;