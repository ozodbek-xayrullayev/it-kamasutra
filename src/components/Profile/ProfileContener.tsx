import React, {Component} from 'react';
import Profile from "./Profile";
import {connect} from "react-redux";
import {getUsersProfile, getStatus,updateStatus,savePhoto,saveProfile} from "../../Redux/profile-reducer"
import {State} from "../../Redux/storeTypes";
import { withRouter} from "react-router-dom"
import WithAurhRedirect from "../../hoc/withAurhRedirect";


type Props = any

class ProfileContener extends React.Component<Props> {
    refreshProfile(){
        let userId = this.props.match.params.userId
        if (!userId) {
            userId = this.props.authorizedUserId;
            // if (!userId){
            //     this.props.history.push('/login')
            // }
        }
        this.props.getUsersProfile(userId||2);
        this.props.getStatus(userId);
    }
    componentDidMount() {
        this.refreshProfile()


    }
    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<{}>, snapshot?: any) {

        if(this.props?.match?.params.userId!==prevProps.props?.match?.params?.userId){
            this.refreshProfile()

        }
    }

    render() {
        // console.log("maadsfsadfsadf")

        return (
            <div>
                <Profile
                    isOwner={!this.props?.match?.params.userId}
                    {...this.props} />
            </div>
        );
    }
}


let mapStateToProps = (state: State) => {
    return({
               profile:state.profilePage.profile,
               status:state.profilePage.status,
               authorizedUserId:state.auth.userId,
               isAuth: state.auth.isAuth,

           })


}
 let AuthRedirectConponent=WithAurhRedirect(ProfileContener)
 let withUrlDataContainerComponent=withRouter(AuthRedirectConponent)


export default connect(mapStateToProps, {getUsersProfile,getStatus,updateStatus,savePhoto,saveProfile})
(withUrlDataContainerComponent)
// export default connect(mapStateToProps,
//     {getUsersProfile}
// )(withUrlDataContainerComponent);
