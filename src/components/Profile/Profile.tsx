import React from 'react';
import "./Profile.scss"
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import MyPostsContainer from "./MyPosts/MyPostsContainer";
import {Redirect} from "react-router-dom"

const Profile=(props:any)=>{
    if(!props.isAuth)return <Redirect to="/login"/>

    return (
        <div>
            <ProfileInfo {...props}/>
            <MyPostsContainer
            />
        </div>
    );
}

export default Profile;