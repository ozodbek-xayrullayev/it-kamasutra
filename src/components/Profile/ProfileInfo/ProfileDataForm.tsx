import React from "react";
import { Form, Input, InputNumber, Button,Checkbox } from 'antd';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};

const ProfileDataForm = ({onSubmit,initialValues,error}:any) => {
    console.log(initialValues)
    const onFinish = (values: any) => {
        onSubmit(values)
    };

    return (
        <Form initialValues={initialValues} {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
            <Form.Item name="fullName" label="Full Name" >
                <Input />
            </Form.Item>
            <Form.Item name="lookingForAJob" label="Looking for a job " >
                <Checkbox></Checkbox>
            </Form.Item>
            <Form.Item name="lookingForAJobDescription"  label="My Scils" >
                <Input.TextArea />
            </Form.Item>
            <Form.Item name="aboutMe" label="About me" >
                <Input.TextArea />
            </Form.Item>
            {Object.keys(initialValues.contacts).map((keys: string) => {
                return  <Form.Item key={keys} name={keys}  label={keys} >
                    <Input placeholder={keys} />
                </Form.Item>
            })}
            {/*<Form.Item name={['user', 'website']} label="Website">*/}
            {/*    <Input />*/}
            {/*</Form.Item>*/}
            {/*<Form.Item name={['user', 'introduction']} label="Introduction">*/}
            {/*    <Input.TextArea />*/}
            {/*</Form.Item>*/}
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button  type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
};

export default ProfileDataForm