import React, {useState} from 'react';
import ProfileStatus from "./ProfileStatus";
import {updateStatus} from "../../../Redux/profile-reducer";
import ProfileStatusWithHooks from "./ProfileStatusWithHooks";
import {DiVim} from "react-icons/all";
import ProfileDataForm from "./ProfileDataForm";
import ProfileDataFormReduxForm from "./ProfileDataForm";

const ProfileInfo = (props: any) => {
    const [editMode, setEditModeMode] = useState<boolean>(false)
    const [status, setStatus] = useState<any>(props.status)
    if (!props.profile) {
        return <span className='spinner-border'/>
    }
    const mainPhotoSelected = (e: any) => {
        if (e.target.files.length) {
            props.savePhoto(e.target.files[0])
        }

    }
    const onSubmit=(formData:any)=>{
        props.saveProfile(formData)
        setEditModeMode(false)

    }
    console.log(props.profile)
    return (
        <div className='m-3'>
            <div>
                <img
                    src="https://cars.usnews.com/pics/size/640x420/static/images/article/202002/128389/1_title_2020_kia_optima.jpg"
                    alt=""/>
            </div>
            <img src={props.profile.photos.large} alt=""/>
            <div>
                {props.isOwner && <input type={"file"} onChange={mainPhotoSelected}/>}
            </div>
            <ProfileStatusWithHooks updateStatus={updateStatus} status={props.status}/>
            {editMode?<ProfileDataForm
                initialValues={props.profile}
                    onSubmit={onSubmit}/>:
                <ProfileData
                    gotEditMode={()=>setEditModeMode(true)}
                    isOwner={props.isOwner} profile={props.profile}/>}


        </div>
    );
}
const ProfileData =(props:any) => {
    const profile=props.profile
    const isOwner=props.isOwner
    const gotEditMode=props.gotEditMode
    return <div>
        <div>
            {isOwner&&<button
                onClick={gotEditMode}
            >
                edit
            </button>}
        </div>

        <div>
            <b>Looking for job</b>: {profile.lookingForAJob ? "yes" : "No"}
        </div>
        {profile.lookingForAJob &&
        <div>
            <b>My professional skills</b>: {profile.lookingForAJobDescription}
        </div>}


        <div>
            <b> This is user status</b>
        </div>
        <div className='h3 m-2'>
            {profile.fullName}
            {!profile.lookingForAJob ?
                <span className="ml-4 rounded bg-danger border-dark spinner-grow"/> :
                <span className=' bg-dark border-primary ml-4 spinner-border'/>
            }
        </div>
        <div className='h4 m-2'>
            <span> {profile.aboutMe}</span>
        </div>

        <h5>
            {profile.lookingForAJobDescription}
        </h5>
        <div className='text-info d-block'>
            <b>Contacts:</b>
            {Object.keys(profile.contacts).map((keys: string) => {
                return <div><b>{keys}</b></div>
            })}
        </div>


    </div>
}


export default ProfileInfo;