import React, {ChangeEvent, Component} from 'react';
import * as fs from "fs";

type PropsType = {
    status:string
    updateStatus:(newStatus:string)=>void


}
type StateType = {
    editMode:boolean
    status:string

}

class ProfileStatus extends Component <PropsType,StateType> {
    state = {
         editMode: true,
        status:this.props.status

    }
    activateditMode=()=>{
        this.setState({
             editMode:false
        })
    }
    deActivateditMode=()=>{
        this.setState({
             editMode:true
        })
        this.props.updateStatus(this.state.status)
    }
    onChange=(e:ChangeEvent<HTMLInputElement>)=>{
        this.setState({
            status:e.target.value
        })

    }
    componentDidUpdate(prevProps: Readonly<PropsType>, prevState: Readonly<StateType>, snapshot?: any) {
        if(prevProps.status!==this.props.status){
            this.setState({
                status:this.props.status?"yes it has ":"no it has not",
                 editMode:true
            })
        }
    }

    render() {

        return (
            <div className='mt-3'>
                {
                    this.state. editMode ?
                        <div>
                            <span onDoubleClick={this.activateditMode}>{this.props.status}</span>
                        </div> :
                        <div>
                            <input onChange={(e:any)=>{this.onChange(e)}}
                                   autoFocus={true} onBlur={this.deActivateditMode} value={this.state.status}/>
                        </div>
                }


            </div>
        );
    }
}

export default ProfileStatus;