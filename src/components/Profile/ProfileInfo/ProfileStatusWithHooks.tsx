import React, {Component, useEffect, useState} from 'react';
import * as fs from "fs";


const ProfileStatusWithHooks = (props: any) => {
    const [editMode, seteditModeMode] = useState<boolean>(true)
    const [status, setStatus] = useState<any>(props.status)
    useEffect(() => {

        setStatus(props.status)
    },[props.status])

    const activateditMode = () => {
        seteditModeMode(false)
    }
    const deActivateditMode = () => {
        seteditModeMode(true)
        props.updateStatus(status)
    }
    const onStatusChange = (e: any) => {
        setStatus(e.currentTarget.value)
    }

    return (
        <div className='mt-3'>

            {
                editMode ?
                    <div>
                        <span onDoubleClick={activateditMode}>{props.status ? props.status : "no data"}</span>
                    </div> :
                    <div>
                        <input
                            onChange={onStatusChange}
                            autoFocus={true}
                            onBlur={deActivateditMode}
                            value={status}
                        />
                    </div>
            }
        </div>
    );
}

export default ProfileStatusWithHooks;