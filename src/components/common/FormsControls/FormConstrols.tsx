import React from "react";
import {Field} from "redux-form";
import {required} from "../../../utils/validators/validators";

export const Textarea = (props: any) => {
    let isError=props.meta.touched && props.meta.error
    const fetch=(props:any)=>{
        if(props.type==="text"||props.type==="password"){
            return <input {...props.input} {...props}/>
        }else{
            return <textarea {...props.input} {...props}/>
        }
    }

    return (
        <div className={"formControl" + " error" &&isError}>
            {fetch(props)}
            <div>
                <span>{props.error}</span>


                {isError&&
            <span className="border border-danger text-danger">{props.meta.error}</span>}
            </div>
        </div>
    )
}
export const createField=(placeholder:string|null,name:any,validate:any[],component:any,type:string)=>{
    return<div>
        <Field
            placeholder={placeholder}
            name={name}
            type={type}
            component={component||null}
            validate={validate}
        />

    </div>
}
